package com.epam.file;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Andrey_Vaganov on 12/5/2016.
 */
public class MainReader {

    /**
     * Формат даты
     */
    private static final String DATE_FORMAT_PATTERN = "dd.MM.yyyy";

    /**
     * Форматтер, используется для преобразования строк в даты и обратно
     */
    private static SimpleDateFormat dateFormatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);


    /**
     * Точка входа в программу
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {
        System.out.println(new File("file.txt").getAbsolutePath());
        readFile();
    }

    /**
     * Метод для чтения дат из файла
     */
    public static void readFile() throws IOException {
        //Открываем потоки на чтение из файла

        try (FileReader reader = new FileReader("file.txt")) {

            BufferedReader byfReader = new BufferedReader(reader);

            //Читаем первую строку из файла
            String strDate = byfReader.readLine();

            while (strDate != null) {
                //Преобразуем строку в дату
                Date date = null;
                try {
                    date = parseDate(strDate);
                    System.out.printf("%1$td-%1$tm-%1$ty \n", date);
                } catch (ParseException e) {
                    System.err.println("Некорректная дата");
                    continue;
                } finally {
                    strDate = byfReader.readLine();
                }


            }

        }catch (FileNotFoundException exc){
            System.out.println("File not found");
        }
    }

    /**
     * Метод преобразует строковое представление даты в класс Date
     *
     * @param strDate строковое представление даты
     * @return
     */
    public static Date parseDate(String strDate) throws ParseException {
        return dateFormatter.parse(strDate);


    }
}
